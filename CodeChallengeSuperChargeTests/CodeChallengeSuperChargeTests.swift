//
//  CodeChallengeSuperChargeTests.swift
//  CodeChallengeSuperChargeTests
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import XCTest
@testable import CodeChallengeSuperCharge

class CodeChallengeSuperChargeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testConcurrentOfflineMode() {
        let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)
        let expect = expectation(description: "the concurrent queue shall succeed")
        
        let callcount = 100
        for item in 1...callcount {
            concurrentQueue.async {
                try! OfflineManagement.shared.saveSearch([searchViewModel(id: item, title: "title for item \(item)")])
            }
            
        }
        
//        while (try? OfflineManagement.shared.loadSearch().last?.id) ?? 0 != callcount {
//            // nop
//        }
//        
        expect.fulfill()
        
        waitForExpectations(timeout: 5) { (error) in
            XCTAssertNil(error, "test failed")
        }
        
    }


}
