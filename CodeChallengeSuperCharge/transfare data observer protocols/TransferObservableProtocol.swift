//
//  TransferObservableProtocol.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation


protocol TransferObservableProtocol {
    func add(_ : TransferObserverProtocol)
    func remove (_ : TransferObserverProtocol)
    func notify()
}
