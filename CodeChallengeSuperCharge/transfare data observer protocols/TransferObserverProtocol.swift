//
//  TransferObserverProtocol.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

protocol TransferObserverProtocol {
    var id: Int? { get }
    func update<T>(_: T)
}
