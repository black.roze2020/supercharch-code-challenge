//
//  MainResult.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct MainResult<T>: Codable where T: Codable {
    
    var page: Int
    
    var resault: [T]
    
    var total_results: Int
    
    var total_pages: Int
    
    
}
