//
//  Movie.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct MovieModel: Codable {
    
    
    var poster_path: String?
    
    var adult: Bool
    
    var overview: String
    
    var release_date: String
    
    var genre_ids: [Int]
    
    var id: Int
    
    var original_title: String
    
    var original_language: String
    
    var title: String
    
    var backdrop_path: String?
    
    var popularity: Int
    
    var vote_count: Int
    
    var video: Bool
    
    var vote_average: Int
    
    
}
