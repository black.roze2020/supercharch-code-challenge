//
//  NetworkErrorModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct NetworkErrorModel: Codable {
    
    var status_message: String
    
    var status_code: Int
}
