//
//  ProductionCountriesModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct ProductionCountriesModel: Codable {
    
    var iso_3166_1: String
    
    var name: String
    
}
