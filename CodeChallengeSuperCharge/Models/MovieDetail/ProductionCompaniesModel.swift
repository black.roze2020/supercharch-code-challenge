//
//  ProductionCompaniesModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct ProductionCompaniesModel: Codable {
    
    var name: String
    
    var id: Int
    
    var logo_path: String?
    
    var origin_country: String
}
