//
//  GenresModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct GenresModel: Codable {
    
    var id: Int
    
    var name: String
    
}
