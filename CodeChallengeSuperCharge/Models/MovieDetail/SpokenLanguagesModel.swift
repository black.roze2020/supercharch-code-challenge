//
//  SpokenLanguagesModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct SpokenLanguagesModel: Codable {
    
    var iso_639_1: String
    
    var name: String
}
