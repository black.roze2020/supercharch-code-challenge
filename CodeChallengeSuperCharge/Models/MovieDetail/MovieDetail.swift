//
//  MovieDetail.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

struct MovieDetail: Codable {
    
    var adult: Bool
    
    var backdrop_path: String?
    
   // var belongs_to_collection: AnyObject?
    
    var budget: Int
    
    var genres: [GenresModel]
    
    var homepage: String?
    
    var id: Int
    
    var imdb_id: String?
    
    var original_language: String
    
    var original_title: String
    
    var overview: String?
    
    var popularity: Int
    
    var poster_path: String?
    
    var production_companies: [ProductionCompaniesModel]
    
    var production_countries: [ProductionCountriesModel]
    
    var release_date: String
    
    var revenue: Int
    
    var runtime: Int?
    
    var spoken_languages: [SpokenLanguagesModel]
    
    var status: String
    
    var tagline: String?
    
    var title: String
    
    var video: Bool
    
    var vote_average: Int
    
    var vote_count: Int
}
