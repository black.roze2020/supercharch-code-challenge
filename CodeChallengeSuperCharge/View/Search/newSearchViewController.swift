//
//  newSearchViewController.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class newSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var searchResult: Observable<[searchViewModel]> = .just([])
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.searchResult = .just([searchViewModel()])
        bindData()
       
        // Do any additional setup after loading the view.
    }
    
    func bindData() {
        
        //        searchResult.bind(to: tableView.rx.items(cellIdentifier: "newSearchTableViewCell")) { (row, data: searchViewModel, cell: newSearchTableViewCell) in
        //            cell.configCell(data)
        //
        //        }.disposed(by: disposeBag)
        //
        searchResult.bindTo(tableView.rx.items) { (tableView, row, element) -> UITableViewCell in
                   
                   if row == 0 {
                       let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTextAreaTableViewCell", for: IndexPath.init(row: row, section: 0)) as! SearchTextAreaTableViewCell
                       cell.add(self)
                       
                       return cell
                   }
                   
                   let cell = tableView.dequeueReusableCell(withIdentifier: "newSearchTableViewCell", for: IndexPath.init(row: row, section: 0)) as! newSearchTableViewCell
                   
                   cell.configCell(element)
                   
                   return cell
               }
    }
    
}


extension newSearchViewController: TransferObserverProtocol {
    var id: Int? {
        return 1
    }
    
    func update<T>(_ obj : T)  {
        if let update = obj as? Observable<[searchViewModel]> {
            update.bind { (object) in
                self.searchResult = Observable.create({ (ret) -> Disposable in
                    var retObject = object
                    retObject.insert(searchViewModel(), at: 0)
                    ret.onNext(retObject)
                    ret.onCompleted()
                    return Disposables.create()
                })
            }
        }
    }
    
}
