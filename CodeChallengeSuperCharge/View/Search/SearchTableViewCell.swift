//
//  SearchTableViewCell.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    var id: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    func cellConfig(_ data: searchViewModel) {
        titleLabel.text = data.titleLabel
        id = data.id
    }
}
