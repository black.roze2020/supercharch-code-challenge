//
//  searchTextAreaObservable.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

extension SearchTextAreaTableViewCell: TransferObservableProtocol {
    
    
    func add(_ item :TransferObserverProtocol) {
        if observers.filter({ $0.id == item.id }).isEmpty {
            observers.append(item)
        }
        
    }
    
    func remove(_ item : TransferObserverProtocol) {
        observers = observers.filter({ $0.id != item.id })
    }
    
    func notify() {
        for item in observers where item.id != nil {
            item.update(retObj)
        }
    }
    
    
}
