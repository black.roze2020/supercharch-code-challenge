//
//  SearchTextAreaTableViewCell.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchTextAreaTableViewCell: UITableViewCell {
  
    var retObj: Observable<[searchViewModel]> = .just([])
    
    var observers: [TransferObserverProtocol] = []
    
    @IBOutlet weak var searchTextField: CustomTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bindElement()
    }
    
    func bindElement() {
        retObj = searchTextField.rx.text
        .orEmpty
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .map {
                $0.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        }
        .flatMap { (query) -> Observable<[searchViewModel]> in
            if query == "" {
                return Observable<[searchViewModel]>.just([])
            }
            let url = UrlGenerator.Search.shared.searchMovies(language: Sentenses.Language.localized(), query: query)
            let modelObj: Observable<MainResult<MovieModel>> = Network().getData(url: url, method: .get, parameters: nil, headers: UrlGenerator.EssentialContent.shared.getHeader())
            var retObj: [searchViewModel] = []
            modelObj.subscribe(onNext: { value in
                for item in value.resault {
                    let viewmodel = searchViewModel(model: item)
                    retObj.append(viewmodel)
                }
                }, onError: { error in
                    retObj = (try? OfflineManagement.shared.loadSearch()) ?? []
                    var offlineMessage = ""
                    if !retObj.isEmpty {
                        offlineMessage = " - Offline mode"
                    }
                    let alert = UIAlertController(title: "Error\(offlineMessage)", message: error.localizedDescription, preferredStyle: .alert)
                    
                    let alertAction = UIAlertAction(title: "Got it!", style: .default, handler: nil)
                    alert.addAction(alertAction)
                    UIApplication.currentViewController()?.present(alert, animated: true, completion: nil)
                  //  self?.present(alert, animated: true, completion: nil)
            }, onCompleted: {
                    print("onCompelete")
                }, onDisposed: nil)
                .disposed(by: DisposeBag())
            
            return Observable.from(optional: retObj)
        }
        
        retObj.bind { [weak self] (_) in
            self?.notify()
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
