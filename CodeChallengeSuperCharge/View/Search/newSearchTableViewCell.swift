//
//  newSearchTableViewCell.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import UIKit

class newSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var movieImage: CustomImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieRankLabel: UILabel!
    @IBOutlet weak var movieLikeLabel: UILabel!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(_ data: searchViewModel) {
        movieImage.image = data.movieImage
        movieTitleLabel.text = data.titleLabel
        movieRankLabel.text = data.rankText
        movieLikeLabel.text = data.rankCountText
        movieDescriptionLabel.text = data.descriptionText
        
    }

}
