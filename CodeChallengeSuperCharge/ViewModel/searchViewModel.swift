//
//  searchViewModel.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class searchViewModel {
    
    var titleLabel: String
    
    var movieImage: UIImage?
    
    var rankText: String
    
    var rankCountText: String
    
    var descriptionText: String
    
    var id: Int
    
    let disposeBag = DisposeBag()
    
    init(model: MovieModel) {
        
        titleLabel = model.title
        
        id = model.id
        
        rankText = "\(model.vote_average)"
        
        rankCountText = "\(model.vote_count)"
        
        descriptionText = model.overview
        
        if let posterPath = model.poster_path {
            fillImage(path: posterPath)
        }
       
        
        
        try? OfflineManagement.shared.saveSearch([self])
    }
    
    func fillImage(path: String) {
            
            Network().getImage(url: path, success: { (image) in
                self.movieImage = image
            }) { (error) in
                print("error: \(error.localizedDescription)")
            }
        
    }
    
    init(id: Int, title: String, image: UIImage, rank: String, rankCount: String, description: String) {
        self.id = id
        self.titleLabel = title
        movieImage = image
        rankText = rank
        rankCountText = rankCount
        descriptionText = description
    }
    
    init() {
        self.id = 0
        self.titleLabel = "dummy"
        rankText = "dummy"
        rankCountText = "dummy"
        descriptionText = "dummy"
    }
    
}
