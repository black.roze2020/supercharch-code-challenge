//
//  OfflineManagment.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class OfflineManagement {
    
    public static let shared = OfflineManagement()
    
    var managedContext : NSManagedObjectContext!
    private let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)
    
    private init () {
        
    }
    @discardableResult
    func saveSearch(_ data: [searchViewModel]) throws -> Bool {
        
       // try concurrentQueue.sync {
        for item in data {
            let fetchRequest = NSFetchRequest<SearchEntity>(entityName: "SearchEntity")
            fetchRequest.predicate = NSPredicate(format: "id = %@", item.id)
            
            
            let fetchResults = try managedContext.fetch(fetchRequest)
            if !fetchResults.isEmpty {
                
                let managedObject = fetchResults[0]
                managedObject.setValue(item.titleLabel, forKey: "title")
                
                try managedContext.save()
                continue
            }
            
            let entity = NSEntityDescription.entity(forEntityName: "SearchEntity", in: managedContext)!
            let searchEntity = SearchEntity(entity: entity, insertInto: managedContext)
            
            searchEntity.id = Int32(item.id)
            searchEntity.title = item.titleLabel
        }
        
        try managedContext.save()
       // }
        return true
    }
    
    
    func loadSearch() throws -> [searchViewModel] {
    
        var retObj: [searchViewModel] = []
        
        try concurrentQueue.sync {
            let fetchRequest = NSFetchRequest<SearchEntity>(entityName: "SearchEntity")
            
            let results = try managedContext.fetch(fetchRequest)
            
            for item in results where item.title != nil {
                // ToDo:  I just fill it with dummy data, and I need to spend more time for develop this postion
                let searchItem = searchViewModel(id: Int(item.id), title: item.title!, image: UIImage(), rank: "3.0", rankCount: "12000", description: "Dummy desc")
                retObj.append(searchItem)
            }
        }
        
        return retObj
    }
    
    
}

