//
//  SearchEntity+CoreDataProperties.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//
//

import Foundation
import CoreData


extension SearchEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SearchEntity> {
        return NSFetchRequest<SearchEntity>(entityName: "SearchEntity")
    }

    @NSManaged public var id: Int32
    @NSManaged public var title: String?

}
