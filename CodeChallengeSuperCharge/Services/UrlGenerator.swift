//
//  UrlGenerator.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation
import Alamofire

class UrlGenerator {
    fileprivate static func baseUrl(_ version: String?) -> String {
        let apiVersion = version ?? "3"
        return "https://developers.themoviedb.org/\(apiVersion)"
    }
    
    
    public final class Movies {
        public static let shared: Movies = Movies()
        
        private func baseUrl(_ version: String?) -> String {
            return "\(UrlGenerator.baseUrl(version))/movies"
        }
        
        public func getMovieDetails(_ movieId: Int, version: String? = nil) -> URL? {
            let strUrl = "\(baseUrl(version))/get-movie-details/\(movieId)"
            return URL(string: strUrl)
        }
    }
    public final class Search {
        
        public static let shared: Search = Search()
        
        private func baseUrl(_ version: String?) -> String {
            return "\(UrlGenerator.baseUrl(version))/search"
        }
        
        public func searchMovies(language: String? = nil, query: String, page: Int? = nil,includeAdult: Bool? = nil,region: String? = nil,year: Int? = nil, primaryReleaseYear: Int? = nil,_ version: String? = nil) -> URL? {
            
            let strUrl = "\(baseUrl(version))/search-movies/query=\(query)\(language != nil ? "&language=\(language!)" : "")\(page != nil ? "&page=\(page!)" : "")\(includeAdult != nil ? "&paginclude_adulte=\(includeAdult!)" : "")\(region != nil ? "&region=\(region!)" : "")\(year != nil ? "&year=\(year!)" : "")\(primaryReleaseYear != nil ? "&primary_release_year=\(primaryReleaseYear!)" : "")"
            return URL(string: strUrl)
        }
    }
    
    public final class EssentialContent {
        
        public static let shared: EssentialContent = EssentialContent()
        
        func getHeader() -> HTTPHeaders {
            let retObj: HTTPHeaders =  ["api_key": "43a7ea280d085bd0376e108680615c7f"]
            return retObj
        }
    }
}
