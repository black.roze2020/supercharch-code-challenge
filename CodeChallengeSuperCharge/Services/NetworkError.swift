//
//  NetworkError.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation


enum NetworkErrors: Error {
    case notFound(errorDescription: String?)
    case error401(errorDescription: String?)
    case nilUrl
    case endOfChain
    case alamofireStatusCodeError
    case convertToDataError
    case alamofireError(errorDescription: String?)
    case unknownError(errorDescription: String?)
}

extension NetworkErrors: LocalizedError {
    var errorDescription: String? {
        switch self {
        
        case .notFound(let errorDescription): return "URL Not found \(String(describing: errorDescription))"
        
        case .error401(let errorDescription): return "An error accures on the server \(String(describing: errorDescription))"
        
        case .endOfChain: return "Undifined error"
        
        case .nilUrl: return "Url was received nil"
        
        case .alamofireStatusCodeError: return "Alamofire can't reach statusCode class Network->\(#function)->\(#line)"
        
        case .convertToDataError: return "Network class can't transform value to dictionary class Network->\(#function)->\(#line)"
            
        case .alamofireError(let errorDescription): return "error when recevie data from API: \(String(describing: errorDescription)) \n class Network->\(#function)->\(#line)"
        
        case .unknownError(let errorDescription): return "new error was occured in line \(#line) , Network->\(#function): \(String(describing: errorDescription))"

        }
    }
}
