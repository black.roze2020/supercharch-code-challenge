//
//  Error401.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

class Error401: NetworkResponseChainProtocol {
    func calculate<T: Codable>(_ unserilized: Data, status: Int) throws -> T {
        if status == 401 {
            if let retObj = try? JSONDecoder().decode(NetworkErrorModel.self, from: unserilized) {
                throw(NetworkErrors.error401(errorDescription: retObj.status_message))
            }
            
            throw(NetworkErrors.error401(errorDescription: ""))
        } else {
            if next != nil {
                return try next!.calculate(unserilized, status: status)
            }
            throw(NetworkErrors.endOfChain)
        }
    }
    var next: NetworkResponseChainProtocol?
}
