//
//  NotResponse404.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

class NotFound404: NetworkResponseChainProtocol {
    func calculate<T: Codable>(_ unserilized: Data, status: Int) throws -> T {
        if status == 404 {
            if let retObj = try? JSONDecoder().decode(NetworkErrorModel.self, from: unserilized) {
                throw(NetworkErrors.notFound(errorDescription: retObj.status_message))
            }
            
            throw(NetworkErrors.notFound(errorDescription: ""))
        } else {
            if next != nil {
                return try next!.calculate(unserilized, status: status)
            }
            throw(NetworkErrors.endOfChain)
        }
    }
    var next: NetworkResponseChainProtocol?
}
