//
//  Network.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import RxSwift
import RxCocoa

class Network {
    
    func getData<T: Codable>(url: URL?, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders?) -> Observable<T> {
        
        Observable<T>.create { (observer) -> Disposable in
            guard let strongUrl = url else {
                
                observer.onError(NetworkErrors.nilUrl)
                observer.onCompleted()
                return Disposables.create()
            }
            var state200: NetworkResponseChainProtocol = Response200()
            var state401: NetworkResponseChainProtocol = Error401()
            let state404: NetworkResponseChainProtocol = NotFound404()
            
            state200.next = state401
            state401.next = state404
            
            AF.request(strongUrl, method: method, parameters: parameters,
                       encoding: encoding, headers: headers)
                .validate()
                .response { response in
                    
                    switch response.result {
                    case let .success(value):
                        if let returnValue = value {
                            if let statusCode = response.response?.statusCode {
                                do {
                                    let value: T = try state200.calculate(returnValue, status: statusCode)
                                    observer.onNext(value)
                                } catch {
                                    if let err = error as? NetworkErrors {
                                        observer.onError(err)
                                    } else {
                                        observer.onError(NetworkErrors.unknownError(errorDescription: error.localizedDescription))
                                    }
                                    
                                }
                                
                            } else {
                                observer.onError(NetworkErrors.alamofireStatusCodeError)
                            }
                        } else {
                            observer.onError(NetworkErrors.convertToDataError)
                            
                        }
                    case let .failure(afError):
                        // handle error message later
                        observer.onError(NetworkErrors.alamofireError(errorDescription: afError.errorDescription))
                    }
            }
            observer.onCompleted()
            return Disposables.create()
            
        }
        
        
        
        
        
        
    }
    
    
    func getImage(url: String, success _success: @escaping(UIImage) -> Void, failure _failure: @escaping(NetworkErrors) -> Void) {
        
        let success: (UIImage) -> Void = { value in
            DispatchQueue.main.async { _success(value) }
        }
        
        let failure: (NetworkErrors) -> Void = { error in
            DispatchQueue.main.async { _failure(error) }
        }
        
        AF.request(url).responseImage { response in
            
            switch response.result {
            case .success(let image):
                success(image)
            case .failure(let err):
                failure(.alamofireError(errorDescription: err.errorDescription))
            }
            
        }
    }
}
