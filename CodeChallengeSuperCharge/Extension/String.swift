//
//  String.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
      let localizedString = NSLocalizedString(self, comment: "")
      return localizedString
    }
}
