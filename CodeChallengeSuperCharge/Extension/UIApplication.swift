//
//  UIApplication.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/30/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import UIKit


extension UIApplication {
    
    
    class func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if #available(iOS 13.0, *) {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                return topController
            // topController should now be your topmost view controller
            }
            return nil
        } else {
            if let nav = base as? UINavigationController {
                return currentViewController(base: nav.visibleViewController)

            } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
                return currentViewController(base: selected)

            } else if let presented = base?.presentedViewController {
                return currentViewController(base: presented)
            }
            return base

        }
        
    }
}
