//
//  Sentenses.swift
//  CodeChallengeSuperCharge
//
//  Created by Mohsen khodadadazadeh on 6/27/20.
//  Copyright © 2020 Mohsen khodadadazadeh. All rights reserved.
//

import Foundation

enum Sentenses: String {
    case Language = "language"
    
    func localized() -> String {
        return self.rawValue.localized()
    }
}
